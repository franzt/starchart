
import sky_area
from input_file import InputFile
from diagram import Diagram
from coord_calc import CoordCalc

f = InputFile('stardata.csv')
area = sky_area.SKY_AREA_ORION

def binarySearch(arr,l, r, x):

    # Check base case
    if r >= l:

        mid = l + (r - l) / 2

        # If element is present at the middle itself
        if  getLen(arr,mid) == x:
            return mid

        # If element is smaller than mid, then it
        # can only be present in left subarray
        elif getLen(arr,mid) > x:
            print(l,r,getLen(arr,mid))
            return binarySearch(arr, l, (mid+l)/2, x)

        # Else the element can only be present
        # in right subarray
        else:
            print(l,r,getLen(arr,mid))
            return binarySearch(arr, (mid +r)/2 , r, x)

    else:
        if(l-r>0.0001):
            print("switch")
            return binarySearch(arr,r,l,x)
        else:
            # Element is not present in the array
            print(l,r,getLen(arr,l))
            return -1

def getLen(sky_Area,mag_min):
    area.mag_min=mag_min
    star_data_list = f.get_stars(area)
    return(len(star_data_list.data))




star_data_list = f.get_stars(area)
res =binarySearch(area,0, 11,3200)
print(res)

star_data_list = f.get_stars(area)

cc = CoordCalc(star_data_list, area, 500)
cc.process()

d = Diagram('My Star Map', area, star_data_list)
list(map(d.add_curve, cc.calc_curves()))
d.render_svg('star-chart.svg')
